package com.example.thibautcornado.applitimer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by thibautcornado on 25/04/2017.
 */

public class DBOpenHelper extends SQLiteOpenHelper {


    public static final String TACHE_KEY = "id";
    public static final String TACHE_TITRE = "titre";
    public static final String TACHE_DATEDEBUT = "dateDebut";
    public static final String TACHE_DATEFIN = "dateFin";
    public static final String TACHE_PRIORITE = "priorite";
    public static final String TACHE_STATUS = "status";
    public static final String TACHE_CATEGORIE = "categorie";
    public static final String TACHE_DESCRIPTION = "description";

    public static final String TACHE_TABLE_NAME = "Tache";
    public static final String TACHE_TABLE_CREATE = "CREATE TABLE "+ TACHE_TABLE_NAME + "(" +
            TACHE_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            TACHE_TITRE + " TEXT NOT NULL," +
            TACHE_DESCRIPTION + " TEXT NOT NULL," +
            TACHE_PRIORITE + " INTEGER NOT NULL," +
            TACHE_DATEDEBUT + " TEXT NOT NULL," +
            TACHE_DATEFIN + " TEXT NOT NULL," +
            TACHE_CATEGORIE + " TEXT NOT NULL," +
            TACHE_STATUS +" INTEGER NOT NULL" +
            ");";

    public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TACHE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
