package com.example.thibautcornado.applitimer;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    GestionnaireTache gestionnaireDesTaches;
    ArrayAdapter<Tache> adaptLV;
    DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button btnClic;
        Spinner sp;
        EditText txtText;
        //Un adapteur pour gérer les données à afficher dans le spinner
        ArrayAdapter<String> adaptSP;

        //Liste de données à passer dans l'adapteur afin de les voir afficher dans le spinner
        ArrayList<String> dataSP = new ArrayList<String>();
        final ArrayList<String> dataLV = new ArrayList<String>();
        dataSP.add("itemSpinner1");
        dataSP.add("itemSpinner2");
        dataSP.add("itemSpinner3");
        dataSP.add("itemSpinner4");
        /*ArrayList<Tache> listeTache = new ArrayList<>();
        listeTache.add(new Tache("tache1", "truc1", "Date1", "Date1", "categorie1", 1, 1));
        listeTache.add(new Tache("tache2", "truc2", "Date2", "Date2", "categorie2", 2, 2));
        listeTache.add(new Tache("tache3", "truc3", "Date3", "Date3", "categorie3", 3, 3));*/
        gestionnaireDesTaches = new GestionnaireTache();

        dbManager = new DBManager(getApplicationContext());
        adaptLV = new ArrayAdapter<Tache>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, gestionnaireDesTaches.getGestionnaireTache()) {
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                TextView txtV = new TextView(getApplicationContext());
                txtV.setText(dbManager.getAllTache().get(position).toString());
                txtV.setTextColor(Color.BLACK);

                return txtV;
            }
        };

        //Le bouton
        btnClic = (Button)findViewById(R.id.buttonCreerTache);
        //Le Spinner
        sp = (Spinner)findViewById(R.id.spinnerCategorie);
        final GridView gridViewTache = (GridView) findViewById(R.id.gridViewTache);

        //Le champ de saisie

        //Initialiser l'adapteur avec la liste de données "data" déclarée plus haut (malgré vide dans un premier temps)
        /*adaptLV = new ArrayAdapter<Tache>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, gestionnaireDesTaches.getGestionnaireTache()) {
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                TextView txtV = new TextView(getApplicationContext());
                txtV.setText(gestionnaireDesTaches.getGestionnaireTache().get(position).toString());
                txtV.setTextColor(Color.BLACK);

                return txtV;
            }
        };*/
        adaptSP = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, dataSP);

        //passer l'adapteur dans le Spinner
        sp.setAdapter(adaptSP);
        gridViewTache.setAdapter(adaptLV);


        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                int index = adapterView.getSelectedItemPosition();
                Toast.makeText(getBaseContext(),"Valeur du spinner est : " + adapterView.getItemAtPosition(adapterView.getPositionForView(view)), Toast.LENGTH_LONG).show();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnClic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, CreationTache.class ), 1);
            }
        });


        gridViewTache.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                Toast.makeText(getBaseContext(),"Long Click monsieur", Toast.LENGTH_LONG).show();

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Suppression")
                        .setMessage("Vulez vous supprimer cette Tache")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                long res = dbManager.sup(gestionnaireDesTaches.getGestionnaireTache().get(position));
                                Toast.makeText(getApplicationContext(), "NBSupp : " + res, Toast.LENGTH_SHORT).show();
                                List<Tache> listNewTache = dbManager.getAllTache();
                                gestionnaireDesTaches.add(listNewTache);
                                adaptLV.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        List<Tache> listNewTache = dbManager.getAllTache();
        gestionnaireDesTaches.add(listNewTache);
        adaptLV.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (resultCode == Activity.RESULT_OK)
        {
            String resultTitre = data.getStringExtra("titre");
            String resultDescription = data.getStringExtra("description");
            String resultDateDebut = data.getStringExtra("dateDebut");
            String resultDateFin = data.getStringExtra("dateFin");
            String resultPeriod = data.getStringExtra("period");
            String resultCategorie = data.getStringExtra("categorie");
            String resultStatue = data.getStringExtra("statut");
            Toast.makeText(getApplicationContext() ,"La tache " + resultTitre +" descrit : "+resultDescription+ " demarre le : " + resultDateDebut + " finis le "+resultDateFin+ " Periode : " + resultPeriod+ " appartient à la categorie : " + resultCategorie + " statut : " + resultStatue ,Toast.LENGTH_LONG ).show();
            gestionnaireDesTaches.AjouterTache(new Tache(resultTitre, resultDescription, resultDateDebut, resultDateFin, resultCategorie, Integer.parseInt(resultPeriod), Integer.parseInt(resultStatue)));
            adaptLV.notifyDataSetChanged();
        }*/


    }
}
