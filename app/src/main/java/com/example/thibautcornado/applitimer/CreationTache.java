package com.example.thibautcornado.applitimer;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class CreationTache extends AppCompatActivity {
    DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation_tache);
        dbManager = new DBManager(getApplicationContext());

        final Switch switchActiveTache;
        final EditText editTextTitre;
        final EditText editTextDescription;
        final EditText dateDebut;
        final EditText dateFin;
        final SeekBar seekBarLevel;
        Button boutonCreationTache;
        GridView gridViewCategorieActiviteCreation;
        final Spinner spinnerCategorieActiviteCreation;

        switchActiveTache= (Switch) findViewById(R.id.switchActivationTache);
        editTextTitre = (EditText) findViewById(R.id.editTextTitre);
        editTextDescription= (EditText) findViewById(R.id.editTextDescription);
        dateDebut = (EditText) findViewById(R.id.editTextDateDebut);
        dateFin = (EditText) findViewById(R.id.editTextDateFin);
        spinnerCategorieActiviteCreation = (Spinner) findViewById(R.id.spinnerCategorieTache);
        boutonCreationTache = (Button) findViewById(R.id.boutonCreationTache);
        seekBarLevel=(SeekBar) findViewById(R.id.seekBarPeriode);

        ArrayAdapter<String> adaptSP;


        //Liste de données à passer dans l'adapteur afin de les voir afficher dans le spinner
        ArrayList<String> dataSP = new ArrayList<String>();
        //ArrayList<Tache> listeTache = new ArrayList<Tache>();

        dataSP.add("itemSpinner1");
        dataSP.add("itemSpinner2");
        dataSP.add("itemSpinner3");
        dataSP.add("itemSpinner4");

        //Initialiser l'adapteur avec la liste de données "data" déclarée plus haut (malgré vide dans un premier temps
        adaptSP = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, dataSP);

        //passer l'adapteur dans le Spinner
        spinnerCategorieActiviteCreation.setAdapter(adaptSP);

        switchActiveTache.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switchActiveTache.isChecked())
                {
                    switchActiveTache.setText("La tache est activé");
                }
                else
                {
                    switchActiveTache.setText("La tache n'est pas activé");
                }
            }
        });

        //Pop-up date
        dateDebut.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog.OnDateSetListener onDataSetListenerDateDebut = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        dateDebut.setText(dayOfMonth + "/" + (month + 1)+ "/" + year);

                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(), onDataSetListenerDateDebut, mYear, mMonth, mDay);
                datePickerDialog.show();
                }
            }
        });

        dateFin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog.OnDateSetListener onDataSetListenerDateFin = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            dateFin.setText(dayOfMonth + "/" +(month +1)+ "/" + year);
                        }
                    };
                    DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(), onDataSetListenerDateFin, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
            }
        });

        boutonCreationTache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCreationTache = new Intent();
                dbManager = new DBManager(getApplicationContext());


                intentCreationTache.putExtra("titre", editTextTitre.getText().toString());
                intentCreationTache.putExtra("description", editTextDescription.getText().toString());
                intentCreationTache.putExtra("dateDebut", dateDebut.getText().toString());
                intentCreationTache.putExtra("dateFin", dateFin.getText().toString());
                intentCreationTache.putExtra("period", "" + seekBarLevel.getProgress());
                intentCreationTache.putExtra("categorie", spinnerCategorieActiviteCreation.getSelectedItem().toString());
                // Toast.makeText(getApplicationContext(), ""+seekBarLevel.getProgress(), Toast.LENGTH_LONG).show();
                String res =  "0";
                if (switchActiveTache.isChecked()) {
                    res = "1";
                }

                Tache tache = new Tache(-1, editTextTitre.getText().toString(), editTextDescription.getText().toString(),
                        dateDebut.getText().toString(), dateFin.getText().toString(),
                        spinnerCategorieActiviteCreation.getSelectedItem().toString(), seekBarLevel.getProgress(), Integer.parseInt(res));
                long resultatInsertion = dbManager.insertTache(tache);
                Toast.makeText(getApplicationContext(), "id de l'insertion est : " + resultatInsertion, Toast.LENGTH_LONG).show();
                //intentCreationTache.putExtra("statut", res);
                //setResult(Activity.RESULT_OK, intentCreationTache);

                finish();
            }
        });

    }
}
