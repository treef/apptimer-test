package com.example.thibautcornado.applitimer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by thibautcornado on 25/04/2017.
 */

public class DBManager {
    public static final String DB_NAME = "DB_TIMER_TACHE";
    public static final int DB_VERSION = 1 ;
    private final DBOpenHelper mdbOpenHelper;

    public DBManager(Context context){
        mdbOpenHelper = new DBOpenHelper(context, DB_NAME, null, DB_VERSION);
    }

    public long insertTache(Tache tache){
        SQLiteDatabase db = mdbOpenHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBOpenHelper.TACHE_TITRE, tache.getTitre());
        values.put(DBOpenHelper.TACHE_DATEDEBUT, tache.getDateDebut());
        values.put(DBOpenHelper.TACHE_DATEFIN, tache.getDateFin());
        values.put(DBOpenHelper.TACHE_DESCRIPTION, tache.getDescription());
        values.put(DBOpenHelper.TACHE_PRIORITE, tache.getPriorite());
        values.put(DBOpenHelper.TACHE_STATUS, tache.getStatut());
        values.put(DBOpenHelper.TACHE_CATEGORIE, tache.getCategorie());

        long newRowId = db.insert(DBOpenHelper.TACHE_TABLE_NAME, null, values);
        db.close();
        return newRowId;
    }

    public long sup(Tache tache) {
        SQLiteDatabase db = mdbOpenHelper.getWritableDatabase();

        String where = DBOpenHelper.TACHE_KEY + " = ? ";
        String[] whereArgs = {String.valueOf(tache.getId()) };

        long res = db.delete(DBOpenHelper.TACHE_TABLE_NAME, where, whereArgs);
        db.close();

        return res;
    }

    public List<Tache> getAllTache(){
        List<Tache> listeTache = new LinkedList<Tache>();
        SQLiteDatabase db = mdbOpenHelper.getReadableDatabase();

        //SELECT
        String[] projection ={
                DBOpenHelper.TACHE_KEY,
                DBOpenHelper.TACHE_TITRE,
                DBOpenHelper.TACHE_DESCRIPTION,
                DBOpenHelper.TACHE_DATEDEBUT,
                DBOpenHelper.TACHE_DATEFIN,
                DBOpenHelper.TACHE_STATUS,
                DBOpenHelper.TACHE_CATEGORIE,
                DBOpenHelper.TACHE_PRIORITE,
        };

        //Clause Where

        //Clause OrderBY
        String sortOrder = DBOpenHelper.TACHE_TITRE + " ASC";
        Cursor cursor = db.query(DBOpenHelper.TACHE_TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                sortOrder);

        while(cursor.moveToNext())
        {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DBOpenHelper.TACHE_KEY));
            String titre = cursor.getString(cursor.getColumnIndexOrThrow(DBOpenHelper.TACHE_TITRE));
            String dateDebut = cursor.getString(cursor.getColumnIndexOrThrow(DBOpenHelper.TACHE_DATEDEBUT));
            String dateFin = cursor.getString(cursor.getColumnIndexOrThrow(DBOpenHelper.TACHE_DATEFIN));
            String description = cursor.getString(cursor.getColumnIndexOrThrow(DBOpenHelper.TACHE_DESCRIPTION));
            int status = cursor.getInt(cursor.getColumnIndexOrThrow(DBOpenHelper.TACHE_STATUS));
            String categorieTache = cursor.getString(cursor.getColumnIndexOrThrow(DBOpenHelper.TACHE_CATEGORIE));
            int prio = cursor.getInt(cursor.getColumnIndexOrThrow(DBOpenHelper.TACHE_PRIORITE));

            Tache tache = new Tache(id, titre, description, dateDebut, dateFin, categorieTache, prio, status);
            listeTache.add(tache);
        }
        cursor.close();
        db.close();
        return listeTache;
    }

}
