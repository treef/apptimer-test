package com.example.thibautcornado.applitimer;

/**
 * Created by thibautcornado on 04/04/2017.
 */

public class Tache {
    private int id;
    private String titre;
    private String description;
    private String dateDebut;
    private String dateFin;
    private int priorite;
    private String categorie;
    private int statut;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public int getPriorite() {
        return priorite;
    }

    public void setPriorite(int priorite) {
        this.priorite = priorite;
    }

    public String getCategorie() {
        return categorie;
    }
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public Tache(){

    }



    public Tache(int id,String titre, String description, String dateDebut, String dateFin, String categorie, int periode, int statut){
        this.setId(id);
        this.setTitre(titre);
        this.setDescription(description);
        this.setDateDebut(dateDebut);
        this.setDateFin(dateFin);
        this.setCategorie(categorie);
        this.setPriorite(periode);
        this.setStatut(statut);
    }

    @Override
    public String toString() {
        String affichageTitre = "La tache " + getTitre() + " id = " + getId();
        String affichageDateDebut = "demarre le : " + getDateDebut();
        String affichageDateFin = "finis le : " + getDateFin();

        return  affichageTitre + "\n" + affichageDateDebut + "\n" + affichageDateFin;
    }
}
