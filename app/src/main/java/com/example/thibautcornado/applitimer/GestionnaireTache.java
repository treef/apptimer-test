package com.example.thibautcornado.applitimer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thibautcornado on 04/04/2017.
 */

public class GestionnaireTache {
    private List<Tache> gestionnaireTache;

    public GestionnaireTache(){
        this.gestionnaireTache = new ArrayList<Tache>();
    }


    public void add(List<Tache> newListTache){
        this.gestionnaireTache.clear();
        this.gestionnaireTache.addAll(newListTache);
    }

    public void AjouterTache(Tache tacheAjoute){
        gestionnaireTache.add(tacheAjoute);
    }

    public List<Tache> getGestionnaireTache() {
        return gestionnaireTache;
    }

    public void setGestionnaireTache(List<Tache> gestionnaireTache) {
        this.gestionnaireTache = gestionnaireTache;
    }
}
